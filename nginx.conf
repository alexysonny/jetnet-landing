map $sent_http_content_type $expires {
    "text/html"                 1h; # set this to your needs
    "text/html; charset=utf-8"  1h; # set this to your needs
    default                     7d; # set this to your needs
}

server {
    listen          80 default_server;             # the port nginx is listening on

    gzip            on;
    gzip_types      text/plain application/xml text/css application/javascript;
    gzip_min_length 1000;

    charset utf-8;

    root /app/html;

    location ~* \.(?:ico|gif|jpe?g|png|woff2?|eot|otf|ttf|svg|js|css|apk)$ {
        expires $expires;
        add_header Pragma public;
        add_header Cache-Control "public";

        try_files $uri $uri/ @proxy;
    }

    location / {
        expires $expires;
        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
        add_header X-Frame-Options "SAMEORIGIN";

        try_files $uri $uri/index.html @proxy; # for generate.subFolders: true
    }

    location @proxy {
        expires $expires;
        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-Cache-Status $upstream_cache_status;

        proxy_redirect                      off;
        proxy_ignore_headers        Cache-Control;
        proxy_http_version          1.1;
        proxy_read_timeout          1m;
        proxy_connect_timeout       1m;
        proxy_pass                  http://jetnet:3000; # set the address of the Node.js instance here
        # proxy_cache                 nuxt-cache;
        # proxy_cache_bypass          $arg_nocache; # probably better to change this
        # proxy_cache_valid           200 302  60m; # set this to your needs
        # proxy_cache_valid           404      1m;  # set this to your needs
        # proxy_cache_lock            on;
        # proxy_cache_use_stale error timeout http_500 http_502 http_503 http_504;
        # proxy_cache_key             $uri$is_args$args;
    }
}

