import { LANG_COOKIE_NAME } from './lang';

export const actions = {
  async nuxtServerInit({ dispatch }, { app, req }) {
    const headers = req && req.headers ? Object.assign({}, req.headers) : {};
    const xForwardedFor = headers['x-forwarded-for'];
    const ip =
      xForwardedFor || req.connection.remoteAddress || req.socket.remoteAddress;

    const ipApi = `http://ip-api.com/json/${ip}`;
    const requestOptions = { method: 'GET', redirect: 'follow' };
    let preferredLang = app.$cookies.get(LANG_COOKIE_NAME);

    if (!preferredLang) {
      try {
        const resp = await fetch(ipApi, requestOptions);
        const data = await resp.json();
        preferredLang = data.country === 'Ukraine' ? 'ua' : 'ru';
      } catch (err) {
        preferredLang = 'ru';
      }
    }

    dispatch('lang/setLang', preferredLang);
  },
};
