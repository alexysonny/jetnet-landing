export const LANG_COOKIE_NAME = 'preferredLang';

const LANG_MUTATIONS = {
  SET_LANG: 'SET_LANG',
};

export const state = () => ({
  lang: '',
});

export const getters = {};

export const actions = {
  setLang({ commit }, lang) {
    const maxAge = 60 * 60 * 24 * 60;

    const now = new Date();
    const expires = new Date(now.setTime(now.getTime() + 60 * 86400000));
    const cookieOptions = {
      sameSite: true,
      maxAge,
      expires,
      path: '/',
    };
    this.$cookies.set(LANG_COOKIE_NAME, lang, cookieOptions);
    this.$i18n.setLocale(lang);
    commit(LANG_MUTATIONS.SET_LANG, lang);
  },
};

export const mutations = {
  [LANG_MUTATIONS.SET_LANG](state, lang) {
    state.lang = lang;
  },
};
