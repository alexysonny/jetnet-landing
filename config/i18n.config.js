export const I18N = {
  lazy: true,
  locales: [
    {
      code: 'ru',
      file: 'ru.js',
      name: 'Русский',
    },
    {
      code: 'ua',
      file: 'ua.js',
      name: 'Українська',
    },
  ],
  strategy: 'no_prefix',
  langDir: 'locales/',
  seo: false,
};
