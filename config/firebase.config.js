export const FIREBASE_CONFIG = {
  config: {
    production: {
      apiKey: 'AIzaSyD-j-g3ISKZuQU3_aokxe2Oo9RDF-shju0',
      authDomain: 'jetnet-da18e.firebaseapp.com',
      databaseURL: 'https://jetnet-da18e.firebaseio.com',
      projectId: 'jetnet-da18e',
      storageBucket: 'jetnet-da18e.appspot.com',
      messagingSenderId: '735134452402',
      appId: '1:735134452402:web:8551bff5074f2d86a317ab',
      measurementId: 'G-MN1H26R74M',
    },
    development: {
      apiKey: 'AIzaSyD-j-g3ISKZuQU3_aokxe2Oo9RDF-shju0',
      authDomain: 'jetnet-da18e.firebaseapp.com',
      databaseURL: 'https://jetnet-da18e.firebaseio.com',
      projectId: 'jetnet-da18e',
      storageBucket: 'jetnet-da18e.appspot.com',
      messagingSenderId: '735134452402',
      appId: '1:735134452402:web:8551bff5074f2d86a317ab',
      measurementId: 'G-MN1H26R74M',
    },
  },
  services: {
    analytics: true,
  },
};
