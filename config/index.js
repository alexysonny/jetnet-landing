import { I18N } from './i18n.config';
import { SENTRY_CONFIG } from './sentry.config';
import { FIREBASE_CONFIG } from './firebase.config';

export {
  // module.exports = {
  I18N,
  SENTRY_CONFIG,
  FIREBASE_CONFIG,
};
