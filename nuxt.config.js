import * as config from './config/index';

require('dotenv').config();

module.exports = {
  mode: 'universal',
  env: {
    buildVersion: process.env.BUILD_VERSION || '1.0',
  },
  /*
   ** Headers of the page
   */
  head() {
    return {
      title: this.$t('meta.title'),
      titleTemplate: this.$t('meta.titleTemplate'),
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        {
          hid: 'description',
          name: 'description',
          content: this.$t('meta.description'),
        },
        { name: 'msapplication-TileColor', content: '#da532c' },
        { name: 'theme-color', content: '#ffffff' },
        {
          property: 'og:image',
          content: 'https://jetnet.app/images/intro-image.png',
        },
        {
          property: 'og:title',
          content: this.$t('meta.ogTitle'),
        },
        {
          property: 'og:description',
          content: this.$t('meta.ogDescription'),
        },
        { property: 'og:locale', content: 'ru_RU' },
        { property: 'og:url', content: 'https://jetnet.app/' },
      ],
      link: [
        { rel: 'canonical', href: 'https://jetnet.app/' },
        {
          rel: 'apple-touch-icon',
          sizes: '180x180',
          href: '/apple-touch-icon.png',
        },
        {
          rel: 'icon',
          type: 'image/png',
          sizes: '32x32',
          href: '/favicon-32x32.png',
        },
        {
          rel: 'icon',
          type: 'image/png',
          sizes: '16x16',
          href: '/favicon-16x16.png',
        },
        { rel: 'manifest', href: '/site.webmanifest' },
        { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#5bbad5' },
      ],
    };
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#3B8070' },
  css: ['aos/dist/aos.css', '~/assets/styles/main.scss'],
  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient, loaders: { vue } }) {
      if (isClient) {
        vue.transformAssetUrls.img = ['data-src', 'src'];
        vue.transformAssetUrls.source = ['data-srcset', 'srcset'];
      }
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
  plugins: [
    { src: '~/plugins/vuelidate' },
    { src: '~/plugins/aos', ssr: false },
    { src: '~/plugins/vue-lazysizes.client.js', ssr: false },
  ],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/sentry',
    ['@nuxtjs/firebase', config.FIREBASE_CONFIG],
    '@nuxtjs/style-resources',
    '@nuxtjs/dotenv',
    ['nuxt-i18n', config.I18N],
    'cookie-universal-nuxt',
  ],

  bootstrapVue: {
    treeShake: true,
    componentPlugins: [
      'FormPlugin',
      'FormGroupPlugin',
      'FormInputPlugin',
      'CollapsePlugin',
      'ModalPlugin',
      'FormTextareaPlugin',
    ],
    directivePlugins: [],
    components: [],
    directives: [],
  },

  sentry: config.SENTRY_CONFIG,

  styleResources: {
    scss: [
      '~bootstrap/scss/bootstrap.scss',
      '~bootstrap-vue/src/index.scss',
      '~/assets/styles/utils/_mixins.scss',
      '~/assets/styles/utils/_variables.scss',
    ],
  },
};
